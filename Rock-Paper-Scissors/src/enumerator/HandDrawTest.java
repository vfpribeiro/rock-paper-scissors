package enumerator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class HandDrawTest {

	@Test
	public void testInitialState() throws Exception {
		assertEquals(HandDraw.wins(HandDraw.PAPER), HandDraw.ROCK);
		assertEquals(HandDraw.wins(HandDraw.ROCK), HandDraw.SCISSORS);
		assertEquals(HandDraw.wins(HandDraw.SCISSORS), HandDraw.PAPER);
	}

}

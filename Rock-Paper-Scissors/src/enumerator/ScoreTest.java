package enumerator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ScoreTest {

	@Test
	public void testInitialState(){
		Integer zero = 0;
		
		assertEquals(Score.wins(Score.ROCKPLAYER), zero);
		assertEquals(Score.wins(Score.RANDOMPLAYER), zero);
		assertEquals(Score.wins(Score.TIE), zero);
	
	}
	
	@Test 
	public void testAddValueOne(){
		Integer one = 1;
		
		Score.addValueOne(Score.ROCKPLAYER);
		Score.addValueOne(Score.RANDOMPLAYER);
		Score.addValueOne(Score.TIE);
		
		assertEquals(Score.wins(Score.ROCKPLAYER), one);
		assertEquals(Score.wins(Score.RANDOMPLAYER), one);
		assertEquals(Score.wins(Score.TIE), one);
	}

}

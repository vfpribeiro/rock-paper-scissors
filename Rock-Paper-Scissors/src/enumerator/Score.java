package enumerator;

import java.util.EnumMap;
import java.util.Map;

public enum Score {
	
	ROCKPLAYER, RANDOMPLAYER, TIE;
	
	private static final Map<Score, Integer> win = new EnumMap<>(Score.class);

	static {
		win.put(ROCKPLAYER, 0);
		win.put(RANDOMPLAYER, 0);
	    win.put(TIE, 0);
	}
	    
	public static Integer wins(Score m) {
		return win.get(m);
	}
	
	public static void addValueOne(Score s){
		win.replace(s, win.get(s), win.get(s)+1);
	}
	
	public static void putValueZero(Score s){
		win.replace(s, win.get(s), 0);
	}
	
}

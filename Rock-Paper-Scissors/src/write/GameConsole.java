package write;

import enumerator.Score;


public class GameConsole implements Writer{

	
	public void putScore() {
		System.out.println("Rock player won: " + Score.wins(Score.ROCKPLAYER));
		System.out.println("Random player won: " + Score.wins(Score.RANDOMPLAYER));
		System.out.println("Tie: " + Score.wins(Score.TIE));
	}

	

}

package players;

import enumerator.HandDraw;



public interface Player {
	public abstract HandDraw getNextHandDraw();
}

package players;

import enumerator.HandDraw;


public class RockPlayer implements Player{
	
	public HandDraw getNextHandDraw() {
		return HandDraw.ROCK;
	}
}

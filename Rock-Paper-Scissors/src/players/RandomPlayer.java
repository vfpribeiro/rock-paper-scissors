package players;

import java.util.Random;

import enumerator.HandDraw;

public class RandomPlayer implements Player {


    private static final HandDraw[] handDraw   = HandDraw.values();

    /**
     * The random number generator used; created once and then cached
     */
    private final Random random;

    public RandomPlayer() {
        random = new Random();
    }

    public HandDraw getNextHandDraw() {
        return handDraw[random.nextInt(handDraw.length)];
    }
}
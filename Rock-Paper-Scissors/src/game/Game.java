package game;

import enumerator.HandDraw;
import enumerator.Score;
import players.Player;
import players.RockPlayer;
import players.RandomPlayer;
import write.GameConsole;
import write.Writer;


public class Game {

	private static final int ROUNDS = 100;
	private Player rockPlayer;
	private Player randomPlayer;
	
	public static void main(String[] args) {
	     Game game = new Game();
	     game.play();
	     Writer writer = new GameConsole();
	     writer.putScore();
	}
	
    public Game() {
		 rockPlayer = new RockPlayer();
	     randomPlayer = new RandomPlayer();
    }
    
    protected void play(){
    	for(int i = 1; i<= ROUNDS; i++ ){
    		evaluate(rockPlayer.getNextHandDraw(),randomPlayer.getNextHandDraw());
    	}
    }

    protected void evaluate(HandDraw rock, HandDraw random){
    	if (HandDraw.wins(rock).equals(random)) {
    		Score.addValueOne(Score.ROCKPLAYER);
    	}
    	else if (HandDraw.wins(random).equals(rock)) {
    		Score.addValueOne(Score.RANDOMPLAYER);
        }
    	else
    		Score.addValueOne(Score.TIE);
    }
    
}

package game;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import enumerator.HandDraw;
import enumerator.Score;

public class GameTest {
	private Game game;
	private Integer one;
	
	
	@Before
	public void setUp() throws Exception {
		game = new Game();
		one = 1;
	}
	
	@Test
	public void testPlay(){
		Integer hundred = 100, total;
		
		game.play();
		total = Score.wins(Score.RANDOMPLAYER) + Score.wins(Score.ROCKPLAYER) + Score.wins(Score.TIE);;
		
		assertEquals(hundred, total);
		
		Score.putValueZero(Score.ROCKPLAYER);
		Score.putValueZero(Score.RANDOMPLAYER);
		Score.putValueZero(Score.TIE);
	}
	
	@Test
	public void testEvaluatePaperRock(){
		game.evaluate(HandDraw.PAPER,HandDraw.ROCK);
		assertEquals(one, Score.wins(Score.ROCKPLAYER));
		Score.putValueZero(Score.ROCKPLAYER);
	}
	
	@Test
	public void testEvaluateRockPaper(){
		game.evaluate(HandDraw.ROCK,HandDraw.PAPER);
		assertEquals(one, Score.wins(Score.RANDOMPLAYER));
		Score.putValueZero(Score.RANDOMPLAYER);
	}
	
	@Test
	public void testEvaluateRockScissors(){
		game.evaluate(HandDraw.ROCK,HandDraw.SCISSORS);
		assertEquals(one, Score.wins(Score.ROCKPLAYER));
		Score.putValueZero(Score.ROCKPLAYER);
	}
	
	@Test
	public void testEvaluateScissorsRock(){
		game.evaluate(HandDraw.SCISSORS,HandDraw.ROCK);
		assertEquals(one, Score.wins(Score.RANDOMPLAYER));
		Score.putValueZero(Score.RANDOMPLAYER);
	}
	
	@Test
	public void testEvaluateScissorsPaper(){
		game.evaluate(HandDraw.SCISSORS,HandDraw.PAPER);
		assertEquals(one, Score.wins(Score.ROCKPLAYER));
		Score.putValueZero(Score.ROCKPLAYER);
	}
	
	@Test
	public void testEvaluatePaperScissors(){
		game.evaluate(HandDraw.PAPER,HandDraw.SCISSORS);
		assertEquals(one, Score.wins(Score.RANDOMPLAYER));
		Score.putValueZero(Score.RANDOMPLAYER);
	}
	
	@Test
	public void testEvaluatePaperPaper(){
		game.evaluate(HandDraw.PAPER,HandDraw.PAPER);
		assertEquals(one, Score.wins(Score.TIE));
		Score.putValueZero(Score.TIE);
	}
	
	@Test
	public void testEvaluateRockRock(){
		game.evaluate(HandDraw.ROCK,HandDraw.ROCK);
		assertEquals(one, Score.wins(Score.TIE));
		Score.putValueZero(Score.TIE);
	}
	@Test
	public void testEvaluateScissorsScissors(){
		game.evaluate(HandDraw.SCISSORS,HandDraw.SCISSORS);
		assertEquals(one, Score.wins(Score.TIE));
		Score.putValueZero(Score.TIE);
	}
	
	@After
	public void tearDown() throws Exception {
	
	}
}
